package edu.towson.cosc431.carpenter.todos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class NewTodoActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TITLE_MESSAGE = "edu.towson.cosc431.carpenter.todos.TITLE";
    public static final String CONTENTS = "edu.towson.cosc431.carpenter.todos.BODY";
    public static final String COMPLETED = "edu.towson.cosc431.carpenter.todos.COMPLETED";
    public static final String DUE_DATE = "edu.towson.cosc431.carpenter.todos.DUE";


    EditText titleInput, contentInput, dueInput;
    CheckBox completedBox;
    Button uploadBtn, saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        titleInput = (EditText) findViewById(R.id.titleInput);
        contentInput = (EditText) findViewById(R.id.contentsInput);
        dueInput = (EditText) findViewById(R.id.dueDate);
        completedBox = (CheckBox) findViewById(R.id.checkBox);
        uploadBtn = (Button) findViewById(R.id.uploadBtn);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);
    }

    public void saveTodo(View v) {
        Intent intent = new Intent();
        String title = titleInput.getText().toString();
        String contents = contentInput.getText().toString();
        boolean todoCompleted = completedBox.isChecked();
        String dueDate = dueInput.getText().toString();
        //sent extras
        intent.putExtra(TITLE_MESSAGE, title);
        intent.putExtra(CONTENTS, contents);
        intent.putExtra(COMPLETED, todoCompleted);
        intent.putExtra(DUE_DATE, dueDate);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveBtn:
                saveTodo(v);
                break;
            case R.id.uploadBtn:
                break;
        }
    }
}
