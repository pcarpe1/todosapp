package edu.towson.cosc431.carpenter.todos;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import edu.towson.cosc431.carpenter.todos.Object.Todo;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    public static final String STATE_KEY = "Todo_Recycler_State";
    Parcelable viewState;
    LinearLayoutManager lm;

    ArrayList<Todo> todoList;

    RecyclerView recyclerView;
    FloatingActionButton addTodoBtn;
    TodoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntent();
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.todoRecycler);
        addTodoBtn = findViewById(R.id.newTodoBtn);
        addTodoBtn.setOnClickListener(this);

        todoList = new ArrayList<Todo>();
        Todo starter = new Todo("Welcome", "Make more To-Do's!!!",
                "", "1/1/2020");
        todoList.add(starter);
        Todo dummy1 = new Todo("Test", "DUMMY TEXT",
                "", "1/1/2022");//starter dummy todo
        Todo[] dummyList = new Todo[9];
        dummyList[0] = dummy1;
        for (int i = 1; i < 8; i++) {//populate recyclerview with dummies
            dummyList[i] = dummy1.duplicate();
            todoList.add(dummyList[i-1]);
        }
        //todoList.add(dummyList[8]);


        //build recyclerview
        adapter = new TodoAdapter(todoList);
        recyclerView.setAdapter(adapter);
        lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.newTodoBtn:
                Intent i = new Intent(this, NewTodoActivity.class);
                startActivityForResult(i, 100);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                String titleTxt = data.getStringExtra(NewTodoActivity.TITLE_MESSAGE);
                String bodyTxt = data.getStringExtra(NewTodoActivity.CONTENTS);
                boolean todoComplete = data.getBooleanExtra(NewTodoActivity.COMPLETED, false);
                String dueDTxt = data.getStringExtra(NewTodoActivity.DUE_DATE);

                Todo todo = new Todo(titleTxt, bodyTxt, "", dueDTxt);
                todo.setCompleted(todoComplete);
                todoList.add(todo);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        viewState = lm.onSaveInstanceState();
        outState.putParcelable(STATE_KEY, viewState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            viewState = savedInstanceState.getParcelable(STATE_KEY);}
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewState != null) {
            lm.onRestoreInstanceState(viewState);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "Portait", Toast.LENGTH_LONG).show();
            adapter.notifyDataSetChanged();
        }
        else if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            Toast.makeText(this, "Landscape", Toast.LENGTH_LONG).show();
            adapter.notifyDataSetChanged();
        }
    }
}
