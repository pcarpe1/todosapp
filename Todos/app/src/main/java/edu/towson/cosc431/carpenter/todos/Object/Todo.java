package edu.towson.cosc431.carpenter.todos.Object;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Todo {
    private String title;
    private String contents;
    private boolean isCompleted;
    private String image;
    private String dateCreated;
    private String dueDate;

    private Date clock = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma");

    public Todo(String title, String contents, String image, String dueDate) {
        this.title = title;
        this.contents = contents;
        this.isCompleted = false;
        this.image = image;
        this.dueDate = dueDate;

        //set date
        dateCreated = dateFormat.format(clock)+ " "+timeFormat.format(clock);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Todo duplicate() {
        Todo another = null;

        try {
            another = (Todo) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            another = new Todo("a", "b", "c", "d");
        }

        return another;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "title='" + title + '\'' +
                ", contents='" + contents + '\'' +
                ", isCompleted=" + isCompleted +
                ", image='" + image + '\'' +
                ", dateCreated='" + dateCreated + '\'' +
                '}';
    }
}
