package edu.towson.cosc431.carpenter.todos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import edu.towson.cosc431.carpenter.todos.Object.Todo;

class TodoAdapter extends RecyclerView.Adapter<TodoViewHolder> implements IController {

    ArrayList<Todo> todoArrayList;

    public TodoAdapter(ArrayList<Todo> todoList) {
        todoArrayList = todoList;
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_item, parent, false);
        TodoViewHolder todoViewHolder = new TodoViewHolder(v, this);
        return todoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        Todo todo = this.todoArrayList.get(position);
        holder.bindToView(todo);

    }

    @Override
    public int getItemCount() {
        return this.todoArrayList.size();
    }

    @Override
    public void deleteTodo(Todo todo) {
        todoArrayList.remove(todo);
        notifyDataSetChanged();
    }

    @Override
    public void editTodo(Todo todo) {

    }

    @Override
    public void toggleCheck(Todo todo) {
        int temp = todoArrayList.indexOf(todo);
        todo.setCompleted(!todo.isCompleted());
        todoArrayList.set(temp, todo);
        notifyDataSetChanged();
    }

}
