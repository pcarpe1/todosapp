package edu.towson.cosc431.carpenter.todos;

import edu.towson.cosc431.carpenter.todos.Object.Todo;

interface IController {
    void deleteTodo(Todo todo);

    void editTodo(Todo todo);

    void toggleCheck(Todo todo);
}
