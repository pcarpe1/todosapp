package edu.towson.cosc431.carpenter.todos;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import edu.towson.cosc431.carpenter.todos.Object.Todo;

class TodoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private final IController controller;
    Todo todo;
    TextView title;
    TextView contents;
    TextView date;
    TextView dueBy;
    Button deleteBtn;
    CheckBox completed;

    public TodoViewHolder(View v, IController controller) {
        super(v);
        v.setOnLongClickListener(this);
        v.setOnClickListener(this);

        this.controller = controller;
        title = v.findViewById(R.id.todoTitle);
        contents = v.findViewById(R.id.todoBody);
        date = v.findViewById(R.id.todoDate);
        dueBy = v.findViewById(R.id.dueByDate);
        deleteBtn = v.findViewById(R.id.deleteButton);
        completed = v.findViewById(R.id.completedBox);

        deleteBtn.setOnClickListener(this);
        completed.setOnClickListener(this);
    }

    public void bindToView (Todo todo) {
        this.todo = todo;
        title.setText(todo.getTitle());
        contents.setText(todo.getContents());
        date.setText(todo.getDateCreated());
        dueBy.setText(todo.getDueDate());
        completed.setChecked(todo.isCompleted());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deleteButton:
                controller.deleteTodo(todo);
                break;
            case R.id.completedBox:
                todo.setCompleted(!todo.isCompleted());
                break;
            case R.id.todoLayout:
                //controller.editTodo(todo);
                controller.toggleCheck(todo);
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        controller.deleteTodo(todo);
        return true;
    }
}
